var mainContainer;
window.addEventListener('load', function() {
  mainContainer = document.querySelector('.main');
  mainContainer.addEventListener("mousewheel", scrollHorizontal, false);
});

function scrollToHash(event) {
  const selectedHash = event.target.hash || event.target.parentElement.hash;
  if (selectedHash) {
    event.preventDefault();
    const hashElement = document.querySelector(selectedHash);
    const hashPosition = hashElement.offsetLeft - 200;
    slimScroller.scroll(hashPosition, 500);
  }
}
function scrollHorizontal(event) {
  if (event.deltaX !== undefined) {
    if (event.deltaX !== 0) return;
    mainContainer.scrollLeft = mainContainer.scrollLeft + event.deltaY;
    event.preventDefault();
  }
  if (event.wheelDelta) {
    mainContainer.scrollLeft = mainContainer.scrollLeft + event.wheelDelta;
    event.preventDefault();
  }
}