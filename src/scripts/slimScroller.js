// Customized for SparkplugVentures.nl

var slimScroller = function (){
    'use strict';

    var elapsed, horizontal;
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame;

    var scroll = function (position, durationTime, callbackFunction) {
        var scrollTargetReference = document.querySelector('.main');
        var duration = durationTime || 500;
        var startPosition = scrollTargetReference.scrollLeft;
        var total = scrollTargetReference.scrollWidth;
        var targetPosition = (position > total) ? total : position;

        var clock = Date.now();
        step(duration, startPosition, targetPosition, clock, scrollTargetReference)();
    };
    var easeInOutCubic = function (time) {
        return (time < 0.5) ? 4 * time * time * time : (time - 1) * (2 * time - 2) * (2 * time - 2) + 1;
    };
    var calculatePosition = function (elapsed, duration, targetPosition, startPosition) {
        return (elapsed > duration) ? targetPosition : startPosition + (targetPosition - startPosition) * easeInOutCubic(elapsed / duration);
    };
    var step = function (duration, startPosition, targetPosition, clock, target) {
        return function() {
            elapsed = Date.now() - clock;
            var stepPosition = calculatePosition(elapsed, duration, targetPosition, startPosition);
    
            target.scrollLeft = stepPosition;
            if (elapsed <= duration) {
                requestAnimationFrame(step(duration, startPosition, targetPosition, clock, target));
            }
        }
    };
    return {
        scroll: scroll
    };
}();