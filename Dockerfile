FROM kyma/docker-nginx
COPY dist/ /var/www
COPY conf/ /etc/nginx/sites-enabled/
EXPOSE 80
CMD 'nginx'
